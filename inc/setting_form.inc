<?php

/**
 * @file
 * Helper file that is handling the backend setting form.
 *
 */

function gridize_setting_form() {

  $form['gridize_width']= array(
      '#type'          => 'textfield',
      '#title'         => t('The width of the layout'),
      '#description'   => t('Enter the width in px like: 960'),
      '#default_value' => variable_get('gridize_width', '960'),
      '#size'          => 4,
      '#required' => TRUE,
      '#maxlength'     => 4,
  );

  $form['gridize_spacer']= array(
      '#type'          => 'textfield',
      '#title'         => t('Set the space between the grids'),
      '#description'   => t('Enter the space in px. like: 10 or enter 0 to disable spacer.'),
      '#default_value' => variable_get('gridize_spacer', '10'),
      '#size'          => 3,
      '#required' => TRUE,
      '#maxlength'     => 3,
  );

  $form['gridize_lhight']= array(
      '#type'          => 'textfield',
      '#title'         => t('Enter the line height of the grid'),
      '#description'   => t('Set this space in px  like: 20.'),
      '#default_value' => variable_get('gridize_lhight', '20'),
      '#size'          => 3,
      '#required' => TRUE,
      '#maxlength'     => 3,
  );

  $form['gridize_color']= array(
      '#type'          => 'textfield',
      '#title'         => t('Enter base color of the grid'),
      '#description'   => t('Input the hex code without the # hash before like: 99CCFF'),
      '#default_value' => variable_get('gridize_color', '99CCFF'),
      '#size'          => 6,
      '#required' => TRUE,
      '#maxlength'     => 6,
  );

  $form['gridize_size'] = array(
      '#type' => 'select',
      '#title' => t('Grid size'),
      '#options' => array(
          '12' => t('12 Grid'),
          '16' => t('16 Grid'),
          '24' => t('24 Grid'),
      ),
      '#default_value' =>  variable_get('gridize_size', '12'),
      '#description' => t('Select the column number - grid size.')
  );

  $form['gridize_align'] = array(
      '#type' => 'select',
      '#title' => t('Alignment'),
      '#options' => array(
          'grid_l' => t('Left'),
          'grid_c' => t('Center'),
          'grid_r' => t('Right'),
      ),
      '#default_value' =>  variable_get('gridize_align', 'grid_c'),
      '#description' => t('Select the grid alignment to be the same as your content.')
  );

  $form['gridize_default'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Enable by default'),
      '#description'   => t('Select to enable by default display of the grid.'),
      '#default_value' => variable_get('gridize_default', TRUE),
  );

  return system_settings_form($form);
}

function gridize_setting_form_validate($form, &$form_state) {
  return (TRUE);
}

function gridize_setting_form_submit($form, &$form_state) {
  system_settings_form_submit($form, $form_state);
  return (TRUE);
}