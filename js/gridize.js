
/**
 * @file
 * This file contains a self calling function which builds up the grid.
 * 
 * The Grid is build up based on the Drupal Backend settings via recursive
 * tag appendition that is then set to the correct sizes.
 *
 */

(function ($) {
    $(document).ready(function() {
// if the tag wans't generated yet
if(!$('#grid_wrapper').length){
	(function(){
		// Value initialization
		var gridize_lhight	= Drupal.settings.gridize_lhight;
		var gridize_spacer	= Drupal.settings.gridize_spacer;
		var gridize_size 	= Drupal.settings.gridize_size;
		var gridize_align	= Drupal.settings.gridize_align;
		var gridize_width	= Drupal.settings.gridize_width;
		var gridize_color	= Drupal.settings.gridize_color;
		var winHeight 		= $(document).height();

		$('body').append('<div id="grid_wrapper"></div>');
		$('#grid_wrapper').append('<div id="grid_inside"></div>');
		$('#grid_wrapper').append('<div id="grid_toggle">' + Drupal.t('Grid') + '</div>');
		$('#grid_inside').css('width', gridize_width);

		var cellNr = (Math.floor(winHeight / gridize_lhight)) * (3 * gridize_size);
		console.log(cellNr);
		for(var i = 1 ; i < cellNr+1 ; i++){
			if(i%3 != 2){
				$('#grid_inside').append('<div class="margin cell"></div>');
			} 
			else{
				$("#grid_inside").append('<div class="body cell"></div>');
			}
		}
		// Set alignment
		switch(gridize_align){
		case 'grid_l':
			$('#grid_inside').addClass('left');
		break;
		case 'grid_c': default:
			$('#grid_inside').addClass('center');
		break;
		case 'grid_r':
			$('#grid_inside').addClass('right');
		break;
		}

		// in case of default hiding of the grid
		if(!Drupal.settings.gridize_default){
			$('#grid_inside').hide();
		}

	})();
}
// toggle display of grid
$('#grid_toggle').click(function() {
	$('#grid_inside').toggle();
});



  }); // jquery end
}(jQuery));